id: tracerx-wp
name: TracerX-WP
description: |
  TracerX-WP is a dynamic symbolic execution engine 
  that employs abstraction learning to prune subtrees 
  which are not necessary for traversal, based on their 
  similarity to previously explored paths. It leverages 
  Weakest Precondition interpolation to represent the 
  learned abstractions effectively.

input_languages:
  - C
project_url: https://tracer-x.github.io/
repository_url: https://github.com/tracer-x/TracerX
spdx_license_identifier: Apache-2.0
benchexec_toolinfo_module: "https://gitlab.com/sosy-lab/software/benchexec/-/raw/main/benchexec/tools/tracerx-wp.py"
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - arpitad10j
  - sajjadrsm
  - sanghu1790

maintainers:
  - name: Joxan Jaffar
    orcid: 0000-0001-9988-6144
    institution: National University of Singapore
    country: Singapore
    url: https://www.comp.nus.edu.sg/cs/bio/joxan/

versions:
  - version: "testcomp25"
    doi: "10.5281/zenodo.14349699"
    benchexec_toolinfo_options: []
    required_ubuntu_packages: []
  - version: "testcomp24"
    doi: "10.5281/zenodo.10202605"
    benchexec_toolinfo_options: []
    required_ubuntu_packages: []

competition_participations:
  - competition: "Test-Comp 2025"
    track: "Test Generation"
    tool_version: "testcomp25"
    jury_member:
      name: Joxan Jaffar
      institution: National University of Singapore
      country: Singapore
      url: https://www.comp.nus.edu.sg/cs/bio/joxan/
  - competition: "Test-Comp 2024"
    track: "Test Generation"
    tool_version: "testcomp24"
    jury_member:
      name: Joxan Jaffar
      institution: National University of Singapore
      country: Singapore
      url: https://www.comp.nus.edu.sg/cs/bio/joxan/

techniques:
  - Interpolation

frameworks_solvers:
  - Z3

literature:
  - doi: 10.1007/978-3-031-57259-3_19
    title: "TracerX: Pruning Dynamic Symbolic Execution with Deletion and Weakest Precondition Interpolation (Competition Contribution)"
    year: 2024

