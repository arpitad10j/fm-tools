id: testcov
name: TestCov
input_languages:
  - C
project_url: https://gitlab.com/sosy-lab/software/test-suite-validator
repository_url: https://gitlab.com/sosy-lab/software/test-suite-validator
spdx_license_identifier: Apache-2.0
benchexec_toolinfo_module: benchexec.tools.testcov
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - dbeyer
  - matthiaskettl
  - lemberger

maintainers:
  - orcid: 0000-0001-7365-5030
    name: Matthias Kettl
    institution: LMU Munich
    country: Germany
    url: https://www.sosy-lab.org/people/kettl/
  - orcid: 0000-0003-0291-815X
    name: Thomas Lemberger
    institution: LMU Munich
    country: Germany
    url: https://www.sosy-lab.org/people/lemberger/
versions:
  - version: "testcomp25-clang-formatted"
    doi: 10.5281/zenodo.14544775
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--format', '--verbose', '--no-plots', '--prep-cgroup', '--disable-info-files', '--compiler', 'clang', '--cov-tool', 'llvm-cov', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages:
      - clang
      - clang-format
      - clang-tidy
      - llvm
      - lcov
      - gcc
  - version: "testcomp25-clang-unformatted"
    doi: 10.5281/zenodo.14544775
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--verbose', '--no-plots', '--prep-cgroup', '--disable-info-files', '--compiler', 'clang', '--cov-tool', 'llvm-cov', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages:
      - clang
      - clang-format
      - clang-tidy
      - llvm
      - lcov
      - gcc
  - version: "testcomp25-gcc-formatted"
    doi: 10.5281/zenodo.14544775
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--format', '--verbose', '--no-plots', '--prep-cgroup', '--disable-info-files', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages:
      - clang
      - clang-format
      - clang-tidy
      - llvm
      - lcov
      - gcc
  - version: "testcomp25-gcc-unformatted"
    doi: 10.5281/zenodo.14544775
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--verbose', '--no-plots', '--prep-cgroup', '--disable-info-files', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages:
      - clang
      - clang-format
      - clang-tidy
      - llvm
      - lcov
      - gcc
  - version: "testcomp24"
    doi: 10.5281/zenodo.10203746
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--format', '--verbose', '--no-plots', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages:
      - clang-format
  - version: "testcomp23"
    url: "https://gitlab.com/sosy-lab/test-comp/archives-2023/-/raw/testcomp23/2023/val_testcov.zip"
    benchexec_toolinfo_options: ['--memlimit', '6GB', '--timelimit-per-run', '50', '--verbose', '--no-plots', '--reduction', 'BY_ORDER', '--reduction-output', 'test-suite']
    required_ubuntu_packages: []

competition_participations:
  - competition: "Test-Comp 2025"
    track: "Validation of Test Suites Clang Formatted"
    tool_version: "testcomp25-clang-formatted"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
  - competition: "Test-Comp 2025"
    track: "Validation of Test Suites Clang Unformatted"
    tool_version: "testcomp25-clang-unformatted"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
  - competition: "Test-Comp 2025"
    track: "Validation of Test Suites GCC Formatted"
    tool_version: "testcomp25-gcc-formatted"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
  - competition: "Test-Comp 2025"
    track: "Validation of Test Suites GCC Unformatted"
    tool_version: "testcomp25-gcc-unformatted"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
  - competition: "Test-Comp 2024"
    track: "Validation of Test Suites"
    tool_version: "testcomp24"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/
  - competition: "Test-Comp 2023"
    track: "Validation of Test Suites"
    tool_version: "testcomp23"
    jury_member:
      orcid: 0000-0001-7365-5030
      name: Matthias Kettl
      institution: LMU Munich
      country: Germany
      url: https://www.sosy-lab.org/people/kettl/

techniques: []

frameworks_solvers: []

literature:
  - doi: 10.1109/ASE.2019.00105
    title: "TestCov: Robust Test-Suite Execution and Coverage Measurement"
    year: 2019
