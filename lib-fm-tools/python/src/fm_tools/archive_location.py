# This file is part of lib-fm-tools, a library for interacting with FM-Tools files:
# https://gitlab.com/sosy-lab/benchmarking/fm-tools
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass
from typing import Optional

from fm_tools.download import DownloadDelegate
from fm_tools.zenodo import (
    get_archive_url_from_zenodo_doi,
)


@dataclass(frozen=True)
class ArchiveLocation:
    raw: str
    resolved: Optional[str] = None
    checksum: Optional[str] = None

    def resolve(self, download_delegate=None) -> "ArchiveLocation":
        if self.resolved is not None:
            return self

        delegate = download_delegate or DownloadDelegate()

        resolved, checksum = get_archive_url_from_zenodo_doi(self.raw, delegate)
        return ArchiveLocation(self.raw, resolved, checksum)
