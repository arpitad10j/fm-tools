# This file is part of lib-fm-tools, a library for interacting with FM-Tools files:
# https://gitlab.com/sosy-lab/benchmarking/fm-tools
#
# SPDX-FileCopyrightText: 2024 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from dataclasses import dataclass
from pathlib import Path
from tempfile import (
    NamedTemporaryFile,
)
from typing import List, Optional, Sequence, Tuple

import werkzeug

from fm_tools.archive_location import ArchiveLocation
from fm_tools.benchexec_helper import DataModel
from fm_tools.competition_participation import CompetitionParticipation
from fm_tools.download import (
    DownloadDelegate,
    download_into,
)
from fm_tools.exceptions import (
    EmptyVersionException,
    InvalidDataException,
    MissingKeysException,
    VersionConflictException,
)
from fm_tools.files import unzip
from fm_tools.fmtool import FmTool
from fm_tools.run import Limits, command_line_run, get_executable_path


@dataclass(frozen=True)
class FmImageConfig:
    base_images: Tuple[str, ...]
    full_images: Tuple[str, ...]
    required_packages: Tuple[str, ...]

    def with_fallback(self, image: str | None):
        """
        Returns a new FmImageConfig with the given image as the base image if the base image is not set.
        """

        if image is None:
            return self

        return FmImageConfig(
            self.base_images or (image,),
            self.full_images,
            self.required_packages,
        )


class FmToolVersion:
    """Class representing one version of an FM-Tool and for downloading, installing, and starting a tool.

    fm_tool_version = FmToolVersion(fm_tool, version_id)

    fm_tool: FmTool from which the version is taken.

    version_id: String specifying the version.

    """

    def __init__(
        self,
        fm_tool: FmTool,
        version_id: str,
    ):
        self._fm_tool = fm_tool
        # In some cases a version like 2.1 is interpreted as float 2.1 by the yaml parser.
        # To keep the version as string, we convert it to string here.
        self._version_id = str(version_id)
        self._tool_name_with_version = self._safe_name_from_config()
        self._version_config = self._find_version_config()
        self._options = []
        if "benchexec_toolinfo_options" in self._version_config:
            self._options = self._version_config["benchexec_toolinfo_options"]
        self._archive_location = self._prepare_archive_location()

    def _safe_name_from_config(self) -> str:
        return werkzeug.utils.secure_filename(
            f"{self._fm_tool.name}-{self._version_id}"
        )

    def _find_version_config(self):
        versions = self._fm_tool.versions
        if versions is None:
            versions = []
        tool_configs = [
            config for config in versions if str(config["version"]) == self._version_id
        ]

        if len(tool_configs) < 1:
            raise VersionConflictException(
                f"Version '{self.get_version_id}' not found for tool '{self._fm_tool.name}'."
            )
        if len(tool_configs) > 1:
            raise VersionConflictException(
                f"There are multiple versions '{self._version_id}' in the YAML file for tool '{self._fm_tool.name}'."
            )
        version_config = tool_configs[0]

        if version_config is None:
            raise EmptyVersionException(
                f"Tool '{self._fm_tool.name}' does not have version '{self._version_id}'."
            )
        return version_config

    def _check_tool_sources(self):
        has_doi = "doi" in self._version_config
        has_url = "url" in self._version_config

        if not (has_url or has_doi):
            raise InvalidDataException("URL and DOI of tool version missing.")
        if has_url and has_doi:
            raise InvalidDataException(
                "Two tool archives provided (one by a URL, one by a DOI)."
            )

        return has_url, has_doi

    def _prepare_archive_location(self):
        has_url, has_doi = self._check_tool_sources()

        if has_doi:
            doi = self._version_config["doi"]
            return ArchiveLocation(doi)

        if has_url:
            return ArchiveLocation(
                self._version_config["url"], self._version_config["url"]
            )

    def download_and_install_into(
        self,
        target_dir: Path,
        delegate: DownloadDelegate = None,
        show_loading_bar: bool = True,
    ):
        """
        Downloads and installs the associated archive into `target_dir`.
        The `target_dir` must not be '/' or '/tmp' to avoid accidental deletion of the system.

        """
        delegate = delegate or DownloadDelegate()

        with NamedTemporaryFile("+wb", suffix=".zip", delete=True) as tmp:
            archive = Path(tmp.name)
            self.download_into(
                archive, delegate=delegate, show_loading_bar=show_loading_bar
            )
            return self.install_from(archive, target_dir)

    def download_into(
        self,
        target: Path,
        delegate: DownloadDelegate = None,
        show_loading_bar: bool = True,
    ) -> Path:
        """
        Download the associated archive into the given target.
        The target must be a file.
        Rethrows potential exceptions from the session in the download delegate.

        :exception DownloadUnsuccessfulException: if the response code is not 200
        :return: the path to the downloaded archive
        """

        delegate = delegate or DownloadDelegate()

        download_into(self, target, delegate, show_loading_bar)

    def install_from(self, archive_dir: Path, target_dir: Path):
        return unzip(archive_dir, target_dir)

    # implement abstract methods
    def get_archive_location(self) -> ArchiveLocation:
        return self._archive_location

    def get_fm_tool(self) -> str:
        return self._fm_tool

    def get_version_id(self) -> str:
        return self._version_id

    def get_options(self) -> List[str]:
        return self._options

    def get_tool_name_with_version(self) -> str:
        return self._tool_name_with_version

    def _find_key(self, key, default):
        top_level = self._config.get(key, default)
        return self._version_specific_config.get(key, top_level)

    def get_images(self) -> FmImageConfig:
        # Top level images
        base_images = tuple(self._find_key("base_container_images", tuple()))
        full_images = tuple(self._find_key("full_container_images", tuple()))
        required_packages = tuple(
            self._find_key(
                "required_ubuntu_packages", self._find_key("required_packages", tuple())
            )
        )

        return FmImageConfig(base_images, full_images, required_packages)

    def command_line(
        self,
        tool_dir: Path,
        input_files: Optional[Sequence[Path]] = None,
        working_dir: Optional[Path] = None,
        property: Optional[Path] = None,
        data_model: Optional[DataModel] = None,
        options: Optional[List[str]] = None,
        add_options_from_fm_data: bool = False,
        limits: Optional[Limits] = None,
    ) -> List[str]:
        return command_line_run(
            self,
            tool_dir,
            input_files,
            working_dir,
            property,
            data_model,
            options,
            add_options_from_fm_data,
            limits,
        )

    def get_executable_path(self, tool_dir: Path) -> Path:
        return get_executable_path(self, tool_dir)

    def _check_fm_data_integrity(self):
        # check if the essential tags are present.
        # Essentiality of tags can be defined in a schema.
        essential_tags = {
            "benchexec_toolinfo_module",
            "name",
            "versions",
        }
        diff = essential_tags - self._config.keys()
        if diff:
            raise MissingKeysException(diff)

    @property
    def competition_participations(self) -> CompetitionParticipation:
        return CompetitionParticipation(self)
